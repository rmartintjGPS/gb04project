package es.unex.giiis.asee.wikileagueapp.testfuncionalcu06;



import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EditBuildTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void editBuildTest() throws InterruptedException {
        sleep(3000);
        onView(withId(R.id.buildsFragment)).perform(click());
        onView(withId(R.id.CrearBuildButton)).perform(click());
        onView(withId(R.id.crearBuild)).perform(click());

        onView(withId(R.id.titleBuild)).perform(typeText("tituloPrueba"));
        onView(withId(R.id.Champ)).perform(typeText("champPrueba"));
        onView(withId(R.id.Item1)).perform(typeText("item1Prueba"));
        onView(withId(R.id.Item2)).perform(typeText("item2Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item3)).perform(typeText("item3Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item4)).perform(typeText("item4Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item5)).perform(typeText("item5Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item6)).perform(typeText("item6Prueba"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.submitBuildButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.buildList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonEditBuild);
                button.performClick();
            }
        }));
        onView(withId(R.id.editTitleBuild)).perform(clearText(), typeText("tituloPruebaBuild"));
        onView(withId(R.id.EditChamp)).perform(clearText(), typeText("champPruebaBuild"));
        onView(withId(R.id.EditItem1)).perform(clearText(), typeText("item1PruebaBuild"));
        onView(withId(R.id.EditItem2)).perform(clearText(), typeText("item2PruebaBuild"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.EditItem3)).perform(clearText(), typeText("item3PruebaBuild"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.EditItem4)).perform(clearText(), typeText("item4PruebaBuild"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.EditItem5)).perform(clearText(), typeText("item5PruebaBuild"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.EditItem6)).perform(clearText(), typeText("item6PruebaBuild"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.submitEditBuildButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.buildList))
                .perform(RecyclerViewActions.scrollTo(hasDescendant(withText("tituloPruebaBuild"))))
                .check(matches(hasItem(hasDescendant(withText("tituloPruebaBuild")))));
        onView(withId(R.id.buildList)).check(matches(hasItem(hasDescendant(withText("Campeón: champPruebaBuild")))));
        onView(withId(R.id.buildList)).check(matches(hasItem(hasDescendant(withText("item1PruebaBuild")))));
        onView(withId(R.id.buildList)).check(matches(hasItem(hasDescendant(withText("item2PruebaBuild")))));
        onView(withId(R.id.buildList)).check(matches(hasItem(hasDescendant(withText("item3PruebaBuild")))));
        onView(withId(R.id.buildList)).check(matches(hasItem(hasDescendant(withText("item4PruebaBuild")))));
        onView(withId(R.id.buildList)).check(matches(hasItem(hasDescendant(withText("item5PruebaBuild")))));
        onView(withId(R.id.buildList)).check(matches(hasItem(hasDescendant(withText("item6PruebaBuild")))));

        onView(withId(R.id.buildList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonDeleteBuild);
                button.performClick();
            }
        }));
    }
    @Ignore
    public static Matcher<View> hasItem(Matcher<View> matcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {

            @Override public void describeTo(Description description) {
                description.appendText("has item: ");
                matcher.describeTo(description);
            }

            @Override protected boolean matchesSafely(RecyclerView view) {
                RecyclerView.Adapter adapter = view.getAdapter();
                for (int position = 0; position < adapter.getItemCount(); position++) {
                    int type = adapter.getItemViewType(position);
                    RecyclerView.ViewHolder holder = adapter.createViewHolder(view, type);
                    adapter.onBindViewHolder(holder, position);
                    if (matcher.matches(holder.itemView)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }
}
