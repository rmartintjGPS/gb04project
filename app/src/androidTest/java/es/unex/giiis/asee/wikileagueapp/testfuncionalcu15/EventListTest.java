package es.unex.giiis.asee.wikileagueapp.testfuncionalcu15;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.Login;
import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EventListTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void eventListTest() throws InterruptedException {

        sleep(3000);
        onView(ViewMatchers.withId(R.id.eventsFragment)).perform(click());

        sleep(500);
        onView(withId(R.id.clear)).perform(click());

        onView(withId(R.id.fab)).perform(click());

        onView(withId(R.id.title)).perform(typeText("tituloPrueba"));

        onView(withId(R.id.body)).perform(typeText("cuerpo prueba lista"));

        Espresso.closeSoftKeyboard();

        onView(withId(R.id.submitButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.eventList))
                .perform(RecyclerViewActions.scrollTo(hasDescendant(withText("tituloPrueba"))))
                .check(matches(hasItem(hasDescendant(withText("tituloPrueba")))));
        onView(withId(R.id.eventList)).check(matches(hasItem(hasDescendant(withText("cuerpo prueba lista")))));

        onView(withId(R.id.eventList)).check(matches(hasDescendant(withId(R.id.titleView))));
        onView(withId(R.id.eventList)).check(matches(hasDescendant(withId(R.id.bodyView))));

        onView(withId(R.id.clear)).perform(click());

    }

    @Ignore
    public static Matcher<View> hasItem(Matcher<View> matcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {

            @Override public void describeTo(Description description) {
                description.appendText("has item: ");
                matcher.describeTo(description);
            }

            @Override protected boolean matchesSafely(RecyclerView view) {
                RecyclerView.Adapter adapter = view.getAdapter();
                for (int position = 0; position < adapter.getItemCount(); position++) {
                    int type = adapter.getItemViewType(position);
                    RecyclerView.ViewHolder holder = adapter.createViewHolder(view, type);
                    adapter.onBindViewHolder(holder, position);
                    if (matcher.matches(holder.itemView)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

}
