package es.unex.giiis.asee.wikileagueapp.testfuncionalcu13;


import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;

import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddCommentCompTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void addCommentCompTest() throws InterruptedException {

        onView(ViewMatchers.withId(R.id.buildsFragment)).perform(click());
        onView(withId(R.id.CrearCompButton)).perform(click());
        onView(withId(R.id.crearComp)).perform(click());

        onView(withId(R.id.titleComp)).perform(typeText("tituloPrueba"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.Champ1)).perform(typeText("champ1Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ2)).perform(typeText("champ2Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ3)).perform(typeText("champ3Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ4)).perform(typeText("champ4Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ5)).perform(typeText("champ5Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Comentario)).perform(typeText("Comentario de prueba"));
        Espresso.closeSoftKeyboard();


        onView(withId(R.id.submitCompButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("tituloPrueba")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champ1Prueba")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champ2Prueba")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champ3Prueba")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champ4Prueba")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champ5Prueba")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("Comentario de prueba")))));

        onView(withId(R.id.compList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonDeleteComp);
                button.performClick();
            }
        }));



    }

    @Ignore
    public static Matcher<View> hasItem(Matcher<View> matcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {

            @Override public void describeTo(Description description) {
                description.appendText("has item: ");
                matcher.describeTo(description);
            }

            @Override protected boolean matchesSafely(RecyclerView view) {
                RecyclerView.Adapter adapter = view.getAdapter();
                for (int position = 0; position < adapter.getItemCount(); position++) {
                    int type = adapter.getItemViewType(position);
                    RecyclerView.ViewHolder holder = adapter.createViewHolder(view, type);
                    adapter.onBindViewHolder(holder, position);
                    if (matcher.matches(holder.itemView)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

}
