package es.unex.giiis.asee.wikileagueapp.testfuncionalcu05;




import androidx.test.espresso.Espresso;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EditProfileTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void editProfileTest() throws InterruptedException {
        sleep(3000);
        onView(ViewMatchers.withId(R.id.profileFragment)).perform(click());
        onView(withId(R.id.editProfile)).perform(click());

        onView(withId(R.id.username)).perform(replaceText("pruebaNombre"));
        onView(withId(R.id.rolJungla)).perform(click());
        onView(withId(R.id.biografiaText)).perform(replaceText("pruebaBio"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.guardarButton)).perform(click());

        onView(withText("pruebaNombre")).check(matches(isDisplayed()));
        onView(withText("Jungla")).check(matches(isDisplayed()));
        onView(withText("pruebaBio")).check(matches(isDisplayed()));
    }
}
