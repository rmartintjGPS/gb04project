package es.unex.giiis.asee.wikileagueapp.testfuncionalcu02;

import androidx.test.espresso.Espresso;


import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;



import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class FavChampTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void favChampTest() throws InterruptedException {

        sleep(3000);
        onView(withId(R.id.champsFragment)).perform(click());
        sleep(3000);

        onView(withId(R.id.champList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        sleep(500);

        onView(withId(R.id.botonFav)).perform(click());

        Espresso.pressBack();
        onView(withId(R.id.profileFragment)).perform(click());
        onView(withId(R.id.favorites)).perform(click());

        onView(withId(R.id.favList)).check(matches(hasDescendant(withId(R.id.domainIcon))));
        onView(withId(R.id.favList)).check(matches(hasDescendant(withId(R.id.champIcon))));
        onView(withId(R.id.favList)).check(matches(hasDescendant(withId(R.id.champName))));
        onView(withId(R.id.favList)).check(matches(hasDescendant(withId(R.id.champDomain))));
    }



}

