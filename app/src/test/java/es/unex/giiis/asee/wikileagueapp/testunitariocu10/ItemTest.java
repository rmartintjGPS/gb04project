package es.unex.giiis.asee.wikileagueapp.testunitariocu10;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.giiis.asee.wikileagueapp.model.Item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ItemTest {

    public static Item item;
    public static Item item2;

    @BeforeClass
    public static void initClass() {
        item = new Item();
        item.setFlatArmorMod(10);
        item.setFlatCritChanceMod(0);
        item.setFlatHpPoolMod(250);
        item.setFlatHpRegenMod(15);
        item.setFlatMagicDamageMod(25);
        item.setFlatMovementSpeedMod(20);
        item.setFlatMpPoolMod(0);
        item.setFlatMpRegenMod(0);
        item.setFlatPhysicalDamageMod(10);
        item.setFlatSpellBlockMod(25);
        item.setGoldBase(750);
        item.setGoldPurchasable(true);
        item.setGoldSell(1966);
        item.setGoldTotal(3150);
        item.setId(1);
        item.setImageUrl("hola.com/item1");
        item.setIsTrinket(false);
        item.setName("Espada");
        item.setPercentAttackSpeedMod(0);
        item.setPercentLifeStealMod(0);
        item.setPercentMovementSpeedMod(0);

    }

    @Test
    public void testItem() {
        assertNotNull(item);
    }

    @Test
    public void testItem2() {
        assertNull(item2);
    }

    @Test
    public void testFlatArmorMod() {
        assertNotNull(item.getFlatArmorMod());
        assertNotEquals(item.getFlatArmorMod(), 3, 0);
        assertEquals(item.getFlatArmorMod(), 10, 0);
    }

    @Test
    public void testFlatCritChanceMod() {
        assertNotNull(item.getFlatCritChanceMod());
        assertNotEquals(item.getFlatCritChanceMod(), 25,0);
        assertEquals(item.getFlatCritChanceMod(), 0,0);
    }
    @Test
    public void testFlatHpPoolMod() {
        assertNotNull(item.getFlatHpPoolMod());
        assertNotEquals(item.getFlatHpPoolMod(), 100, 0);
        assertEquals(item.getFlatHpPoolMod(), 250, 0);
    }
    @Test
    public void testFlatHpRegenMod() {
        assertNotNull(item.getFlatHpRegenMod());
        assertNotEquals(item.getFlatHpRegenMod(), 3, 0);
        assertEquals(item.getFlatHpRegenMod(), 15, 0);
    }
    @Test
    public void testFlatMagicDamageMod() {
        assertNotNull(item.getFlatMagicDamageMod());
        assertNotEquals(item.getFlatMagicDamageMod(), 20, 0);
        assertEquals(item.getFlatMagicDamageMod(), 25, 0);
    }
    @Test
    public void testFlatMovementSpeedMod() {
        assertNotNull(item.getFlatMovementSpeedMod());
        assertNotEquals(item.getFlatMovementSpeedMod(), 5, 0);
        assertEquals(item.getFlatMovementSpeedMod(), 20, 0);
    }
    @Test
    public void testFlatMpPoolMod() {
        assertNotNull(item.getFlatMpPoolMod());
        assertNotEquals(item.getFlatMpPoolMod(), 5, 0);
        assertEquals(item.getFlatMpPoolMod(), 0, 0);
    }
    @Test
    public void testMpRegenMod() {
        assertNotNull(item.getFlatMpRegenMod());
        assertNotEquals(item.getFlatMpRegenMod(), 5, 0);
        assertEquals(item.getFlatMpRegenMod(), 0, 0);
    }
    @Test
    public void testFlatPhysicalDamageMod() {
        assertNotNull(item.getFlatPhysicalDamageMod());
        assertNotEquals(item.getFlatPhysicalDamageMod(), 20, 0);
        assertEquals(item.getFlatPhysicalDamageMod(), 10, 0);
    }
    @Test
    public void testFlatSpellBlockMod() {
        assertNotNull(item.getFlatSpellBlockMod());
        assertNotEquals(item.getFlatSpellBlockMod(), 30);
        assertEquals(item.getFlatSpellBlockMod(),25);
    }
    @Test
    public void testGoldBase() {
        assertNotNull(item.getGoldBase());
        assertNotEquals(item.getGoldBase(), 1000, 0);
        assertEquals(item.getGoldBase(), 750, 0);
    }
    @Test
    public void testGoldPurchasable() {
        assertNotNull(item.getGoldPurchasable());
        assertNotEquals(item.getGoldPurchasable(), false);
        assertEquals(item.getGoldPurchasable(), true);
    }
    @Test
    public void testGoldSell() {
        assertNotNull(item.getGoldSell());
        assertNotEquals(item.getGoldSell(), 2345, 0);
        assertEquals(item.getGoldSell(), 1966, 0);
    }
    @Test
    public void testGoldTotal() {
        assertNotNull(item.getGoldTotal());
        assertNotEquals(item.getGoldTotal(), 3330, 0);
        assertEquals(item.getGoldTotal(), 3150, 0);
    }
    @Test
    public void testId() {
        assertNotNull(item.getId());
        assertNotEquals(item.getId(), 3, 0);
        assertEquals(item.getId(), 1, 0);
    }
    @Test
    public void testImageUrl() {
        assertNotNull(item.getImageUrl());
        assertNotEquals(item.getImageUrl(), "hola.com/item34");
        assertEquals(item.getImageUrl(), "hola.com/item1");
    }

    @Test
    public void testIsTrinket() {
        assertNotNull(item.getIsTrinket());
        assertNotEquals(item.getIsTrinket(), true);
        assertEquals(item.getIsTrinket(), false);
    }
    @Test
    public void testName() {
        assertNotNull(item.getName());
        assertNotEquals(item.getName(), "Escudo");
        assertEquals(item.getName(), "Espada");
    }
    @Test
    public void testPercentAttackSpeedMod() {
        assertNotNull(item.getPercentAttackSpeedMod());
        assertNotEquals(item.getPercentAttackSpeedMod(), 20, 0);
        assertEquals(item.getPercentAttackSpeedMod(), 0, 0);
    }
    @Test
    public void testPercentLifeStealMod() {
        assertNotNull(item.getPercentLifeStealMod());
        assertNotEquals(item.getPercentLifeStealMod(), 15, 0);
        assertEquals(item.getPercentLifeStealMod(), 0, 0);
    }
    @Test
    public void testPercentMovementSpeedMod() {
        assertNotNull(item.getPercentMovementSpeedMod());
        assertNotEquals(item.getPercentMovementSpeedMod(), 15, 0);
        assertEquals(item.getPercentMovementSpeedMod(), 0, 0);
    }

    @AfterClass
    public static void finishClass() {
        item = null;
    }

}
