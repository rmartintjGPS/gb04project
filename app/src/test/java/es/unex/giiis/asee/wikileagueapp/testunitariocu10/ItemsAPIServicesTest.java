package es.unex.giiis.asee.wikileagueapp.testunitariocu10;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.wikileagueapp.ChampsServices;
import es.unex.giiis.asee.wikileagueapp.ItemsServices;
import es.unex.giiis.asee.wikileagueapp.model.Champion;
import es.unex.giiis.asee.wikileagueapp.model.Item;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ItemsAPIServicesTest {

    @Test
    public void getChampionTest() throws IOException {

        //Step1: we create champion list
        Item item = new Item();


        item.setFlatArmorMod(10);
        item.setFlatCritChanceMod(0);
        item.setFlatHpPoolMod(250);
        item.setFlatHpRegenMod(15);
        item.setFlatMagicDamageMod(25);
        item.setFlatMovementSpeedMod(20);
        item.setFlatMpPoolMod(0);
        item.setFlatMpRegenMod(0);
        item.setFlatPhysicalDamageMod(10);
        item.setFlatSpellBlockMod(25);
        item.setGoldBase(750);
        item.setGoldPurchasable(true);
        item.setGoldSell(1966);
        item.setGoldTotal(3150);
        item.setId(1);
        item.setImageUrl("hola.com/item1");
        item.setIsTrinket(false);
        item.setName("Espada");
        item.setPercentAttackSpeedMod(0);
        item.setPercentLifeStealMod(0);
        item.setPercentMovementSpeedMod(0);


        //Item dos
        Item item2 = new Item();


        item2.setFlatArmorMod(0);
        item2.setFlatCritChanceMod(25);
        item2.setFlatHpPoolMod(0);
        item2.setFlatHpRegenMod(0);
        item2.setFlatMagicDamageMod(0);
        item2.setFlatMovementSpeedMod(25);
        item2.setFlatMpPoolMod(125);
        item2.setFlatMpRegenMod(15);
        item2.setFlatPhysicalDamageMod(55);
        item2.setFlatSpellBlockMod(30);
        item2.setGoldBase(850);
        item2.setGoldPurchasable(true);
        item2.setGoldSell(1750);
        item2.setGoldTotal(3500);
        item2.setId(2);
        item2.setImageUrl("hola.com/item2");
        item2.setIsTrinket(false);
        item2.setName("Daga");
        item2.setPercentAttackSpeedMod(20);
        item2.setPercentLifeStealMod(10);
        item2.setPercentMovementSpeedMod(20);

        List<Item> itemList = new ArrayList<>();
            itemList.add(item);
            itemList.add(item2);

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
            mockedResponse.setResponseCode(200);
            mockedResponse.setBody(objectMapper.writeValueAsString(itemList));
            mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        ItemsServices service = retrofit.create(ItemsServices.class);


        //Step6: we create the call to get centers
        Call<List<Item>> call = service.listItems();
        // and we execute the call
        Response<List<Item>> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response !=null);

        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Item> itemsResponse = response.body();

        assertFalse(itemsResponse.isEmpty());

        assertTrue(itemsResponse.size()==2);

        assertEquals(item.getFlatArmorMod(), 10, 0);

        assertEquals(item.getFlatCritChanceMod(), 0,0);

        assertEquals(item.getFlatHpPoolMod(), 250, 0);

        assertEquals(item.getFlatHpRegenMod(), 15, 0);

        assertEquals(item.getFlatMagicDamageMod(), 25, 0);

        assertEquals(item.getFlatMovementSpeedMod(), 20, 0);

        assertEquals(item.getFlatMpPoolMod(), 0, 0);

        assertEquals(item.getFlatMpRegenMod(), 0, 0);

        assertEquals(item.getFlatPhysicalDamageMod(), 10, 0);

        assertEquals(item.getFlatSpellBlockMod(),25);

        assertEquals(item.getGoldBase(), 750, 0);

        assertEquals(item.getGoldPurchasable(), true);

        assertEquals(item.getGoldSell(), 1966, 0);

        assertEquals(item.getGoldTotal(), 3150, 0);

        assertEquals(item.getId(), 1, 0);

        assertEquals(item.getImageUrl(), "hola.com/item1");

        assertEquals(item.getIsTrinket(), false);

        assertEquals(item.getName(), "Espada");

        assertEquals(item.getPercentAttackSpeedMod(), 0, 0);

        assertEquals(item.getPercentLifeStealMod(), 0, 0);

        assertEquals(item.getPercentMovementSpeedMod(), 0, 0);




        assertEquals(item2.getFlatArmorMod(), 0, 0);

        assertEquals(item2.getFlatCritChanceMod(), 25,0);

        assertEquals(item2.getFlatHpPoolMod(), 0, 0);

        assertEquals(item2.getFlatHpRegenMod(), 0, 0);

        assertEquals(item2.getFlatMagicDamageMod(), 0, 0);

        assertEquals(item2.getFlatMovementSpeedMod(), 25, 0);

        assertEquals(item2.getFlatMpPoolMod(), 125, 0);

        assertEquals(item2.getFlatMpRegenMod(), 15, 0);

        assertEquals(item2.getFlatPhysicalDamageMod(), 55, 0);

        assertEquals(item2.getFlatSpellBlockMod(),30);

        assertEquals(item2.getGoldBase(), 850, 0);

        assertEquals(item2.getGoldPurchasable(), true);

        assertEquals(item2.getGoldSell(), 1750, 0);

        assertEquals(item2.getGoldTotal(), 3500, 0);

        assertEquals(item2.getId(), 2, 0);

        assertEquals(item2.getImageUrl(), "hola.com/item2");

        assertEquals(item2.getIsTrinket(), false);

        assertEquals(item2.getName(), "Daga");

        assertEquals(item2.getPercentAttackSpeedMod(), 20, 0);

        assertEquals(item2.getPercentLifeStealMod(), 10, 0);

        assertEquals(item2.getPercentMovementSpeedMod(), 20, 0);


        //Step9: Finish web server
        mockWebServer.shutdown();


    }

    @Test
    public void getEmptyItemsTest() throws IOException {

        //Step1: we create empty champions list
        List<Item> items = new ArrayList<>();


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only get list items request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(items));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        ItemsServices service = retrofit.create(ItemsServices.class);



        //Step6: we create the call to get items
        Call<List<Item>> call = service.listItems();
        // and we execute the call
        Response<List<Item>> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Item> itemsResponse =response.body();
        assertTrue(itemsResponse.isEmpty());

        //Step9: Finish web server
        mockWebServer.shutdown();
    }

}
