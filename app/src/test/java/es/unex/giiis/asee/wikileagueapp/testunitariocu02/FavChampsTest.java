package es.unex.giiis.asee.wikileagueapp.testunitariocu02;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class FavChampsTest {

    public static FavChamp favc;
    public static FavChamp favc2;

    @BeforeClass
    public static void initClass() {
        favc = new FavChamp(5,"","", "");
        favc.setName("NASUS");
        favc.setDominio("Legendario");
        favc.setId(1);
        favc.setImage("imagenCampeon.jpg");
    }

    @Test
    public void testFavChamps() {
        assertNotNull(favc);
    }

    @Test
    public void testFavChamps2() {
        assertNull(favc2);
    }

    @Test
    public void testname() {
        assertNotNull(favc.getName());
        assertNotEquals(favc.getName(), "Juanito");
        assertEquals(favc.getName(), "NASUS");
    }

    @Test
    public void testdominio() {
        assertNotNull(favc.getDominio());
        assertNotEquals(favc.getDominio(), "Malo");
        assertEquals(favc.getDominio(), "Legendario");
    }

    @Test
    public void testid() {
        assertNotNull(favc.getId());
        assertNotEquals(favc.getId(), 3);
        assertEquals(favc.getId(), 1);
    }

    @Test
    public void testimagen() {
        assertNotNull(favc.getImage());
        assertNotEquals(favc.getImage(), "cargando.png");
        assertEquals(favc.getImage(), "imagenCampeon.jpg");
    }

    @AfterClass
    public static void finishClass() {
        favc = null;
    }
}