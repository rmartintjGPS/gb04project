package es.unex.giiis.asee.wikileagueapp.testunitariocu09;

import android.renderscript.Long3;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.giiis.asee.wikileagueapp.model.Champion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ChampionTest {

    public static Champion champ;
    public static Champion champ2;

    @BeforeClass
    public static void initClass() {
        champ = new Champion();
        champ.setArmor(30.0);
        champ.setArmorperlevel(2.0);
        champ.setAttackdamage(60.0);
        champ.setAttackdamageperlevel(3.0);
        champ.setAttackrange(550.0);

        champ.setAttackspeedperlevel(10.0);
        champ.setBigImageUrl("hola.com");
        champ.setCrit(10.0);
        champ.setCritperlevel(0.0);
        champ.setHp(475.0);
        champ.setHpperlevel(15.0);
        champ.setHpregen(8.5);
        champ.setHpregenperlevel(2.0);
        champ.setId(1);
        champ.setImageUrl("hola.com/campeon");

        champ.setMovespeed(375.0);
        champ.setMp(350.0);
        champ.setMpperlevel(25.0);
        champ.setMpregen(15.0);
        champ.setMpregenperlevel(5.5);
        champ.setName("Ezreal");
        champ.setSpellblock(45.0);
        champ.setSpellblockperlevel(3.5);
    }

    @Test
    public void testChamp1() {
        assertNotNull(champ);
    }

    @Test
    public void testChamps2() {
        assertNull(champ2);
    }
    @Test
    public void testarmor() {
        assertNotNull(champ.getArmor());
        assertNotEquals(champ.getArmor(), 15.0);
        assertEquals(champ.getArmor(), 30.0, 0.0);
    }


    @Test
    public void testarmorPerLevel() {
        assertNotNull(champ.getArmorperlevel());
        assertNotEquals(champ.getArmorperlevel(), 1.0);
        assertEquals(champ.getArmorperlevel(), 2.0, 0.0);
    }
    @Test
    public void testAttackdamage() {
        assertNotNull(champ.getAttackdamage());
        assertNotEquals(champ.getAttackdamage(), 50.0);
        assertEquals(champ.getAttackdamage(), 60.0, 0.0);
    }
    @Test
    public void testAttackdamageperlevel() {
        assertNotNull(champ.getAttackdamageperlevel());
        assertNotEquals(champ.getAttackdamageperlevel(), 5.0);
        assertEquals(champ.getAttackdamageperlevel(), 3.0, 0.0);
    }
    @Test
    public void testAttackrange() {
        assertNotNull(champ.getAttackrange());
        assertNotEquals(champ.getAttackrange(), 650.0);
        assertEquals(champ.getAttackrange(), 550.0, 0.0);
    }

    @Test
    public void testAttackspeedperlevel() {
        assertNotNull(champ.getAttackspeedperlevel());
        assertNotEquals(champ.getAttackspeedperlevel(), 12.0);
        assertEquals(champ.getAttackspeedperlevel(), 10.0, 0.0);
    }

    @Test
    public void testBigImageUrl() {
        assertNotNull(champ.getBigImageUrl());
        assertNotEquals(champ.getBigImageUrl(), "adios.com");
        assertEquals(champ.getBigImageUrl(), "hola.com");
    }

    @Test
    public void testCrit() {
        assertNotNull(champ.getCrit());
        assertNotEquals(champ.getCrit(), 15.0);
        assertEquals(champ.getCrit(), 10.0,0.0);
    }
    @Test
    public void testCritperlevel() {
        assertNotNull(champ.getCritperlevel());
        assertNotEquals(champ.getCritperlevel(), 5.0);
        assertEquals(champ.getCritperlevel(), 0.0, 0.0);
    }
    @Test
    public void testHp() {
        assertNotNull(champ.getHp());
        assertNotEquals(champ.getHp(), 575.0);
        assertEquals(champ.getHp(), 475.0, 0.0);
    }
    @Test
    public void testHpperlevel() {
        assertNotNull(champ.getHpperlevel());
        assertNotEquals(champ.getHpperlevel(), 12.0);
        assertEquals(champ.getHpperlevel(), 15.0, 0.0);
    }


    @Test
    public void testHpregen() {
        assertNotNull(champ.getHpregen());
        assertNotEquals(champ.getHpregen(), 5.0);
        assertEquals(champ.getHpregen(), 8.5, 0.0);
    }


    @Test
    public void testHpregenperlevel() {
        assertNotNull(champ.getHpregenperlevel());
        assertNotEquals(champ.getHpregenperlevel(), 3.0);
        assertEquals(champ.getHpregenperlevel(), 2.0, 0.0);
    }


    @Test
    public void testid() {
        assertNotNull(champ.getId());
        assertNotEquals(champ.getId(), new Integer(3));
        assertEquals(champ.getId(), new Integer(1));
    }


    @Test
    public void testImageUrl() {
        assertNotNull(champ.getImageUrl());
        assertNotEquals(champ.getImageUrl(), "adios.com/campeon");
        assertEquals(champ.getImageUrl(), "hola.com/campeon");
    }

    @Test
    public void testMovespeed() {
        assertNotNull(champ.getMovespeed());
        assertNotEquals(champ.getMovespeed(), 350.0);
        assertEquals(champ.getMovespeed(), 375.0, 0.0);
    }

    @Test
    public void testMp() {
        assertNotNull(champ.getMp());
        assertNotEquals(champ.getMp(), 500.0);
        assertEquals(champ.getMp(), 350.0, 0.0);
    }

    @Test
    public void testMpperlevel() {
        assertNotNull(champ.getMpperlevel());
        assertNotEquals(champ.getMpperlevel(), 12.0);
        assertEquals(champ.getMpperlevel(), 25.0, 0.0);
    }
    @Test
    public void testMpregen() {
        assertNotNull(champ.getMpregen());
        assertNotEquals(champ.getMpregen(), 5.5);
        assertEquals(champ.getMpregen(), 15.0, 0.0);
    }
    @Test
    public void testMpregenperlevel() {
        assertNotNull(champ.getMpregenperlevel());
        assertNotEquals(champ.getMpregenperlevel(), 3.0);
        assertEquals(champ.getMpregenperlevel(), 5.5, 0.0);
    }
    @Test
    public void testName() {
        assertNotNull(champ.getName());
        assertNotEquals(champ.getName(), "Paco");
        assertEquals(champ.getName(), "Ezreal");
    }

    @Test
    public void testSpellblock() {
        assertNotNull(champ.getSpellblock());
        assertNotEquals(champ.getSpellblock(), 33.0);
        assertEquals(champ.getSpellblock(), 45.0,0.0);
    }

    @Test
    public void testSpellblockperlevel() {
        assertNotNull(champ.getSpellblockperlevel());
        assertNotEquals(champ.getSpellblockperlevel(), 2.5);
        assertEquals(champ.getSpellblockperlevel(), 3.5, 0.0);
    }

    @AfterClass
    public static void finishClass() {
        champ = null;
    }

}