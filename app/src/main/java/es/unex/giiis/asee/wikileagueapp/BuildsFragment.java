package es.unex.giiis.asee.wikileagueapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;
import static android.app.Activity.RESULT_OK;

public class BuildsFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_builds, container, false);
        final Button CrearCompButton = view.findViewById(R.id.CrearCompButton);
        CrearCompButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ListaComps.class);
                startActivity(intent);
            }
        });

        final Button crearBuildButton = view.findViewById(R.id.CrearBuildButton);
        crearBuildButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ListaBuilds.class);
                startActivity(intent);
            }
        });
        return view;
    }
}