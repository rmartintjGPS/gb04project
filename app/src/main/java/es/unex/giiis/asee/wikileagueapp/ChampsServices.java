package es.unex.giiis.asee.wikileagueapp;

import es.unex.giiis.asee.wikileagueapp.model.Champion;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ChampsServices {
    @GET("champions?token=EDZwdB0PpRVDcWMnyoE9Y1NPO7T-a5EBmqVeSac88-ytL5mgLMY")
    Call<List<Champion>> listChamps();
}
