package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Build;

public class EditarBuildVM extends ViewModel {
    private final RoomRepository mRepository;

    public EditarBuildVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
    }

    public void update(Build build) {
        mRepository.updateBuild(build);
    }
}
