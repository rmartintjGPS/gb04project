package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class EditarCompVMFactory extends ViewModelProvider.NewInstanceFactory {
    private final RoomRepository mRepository;
    private final Context context;

    public EditarCompVMFactory(Context context, RoomRepository repository) {
        this.mRepository = repository;
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new EditarCompVM(context, mRepository);
    }
}
