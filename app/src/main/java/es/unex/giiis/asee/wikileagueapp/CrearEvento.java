package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.unex.giiis.asee.wikileagueapp.model.Event;

public class CrearEvento extends AppCompatActivity {

    private EditText mTitleText;
    private EditText mBodyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_evento);

        mTitleText = findViewById(R.id.title);
        mBodyText = findViewById(R.id.body);

        final Button cancelButton =  findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent data = new Intent();
                setResult(RESULT_CANCELED, data);
                finish();

            }
        });

        final Button submitButton =  findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titleString = mTitleText.getText().toString();
                String bodyString = mBodyText.getText().toString();

                if (!titleString.equals("") && !bodyString.equals("")) {
                    Intent data = new Intent();
                    Event.packageIntent(data, titleString, bodyString);

                    setResult(RESULT_OK, data);
                    finish();
                }
                else {
                    Toast.makeText(CrearEvento.this, "Los campos deben ser rellenados.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}