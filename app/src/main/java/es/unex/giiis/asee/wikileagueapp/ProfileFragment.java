package es.unex.giiis.asee.wikileagueapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ProfileFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);

        TextView username = view.findViewById(R.id.nombreUsuario);
        TextView rol = view.findViewById(R.id.rolPreferido);
        TextView biografia = view.findViewById(R.id.biografia);

        String userData;
        userData = pref.getString("username", "0");

        if((userData.equals("0"))){
            username.setText("Invocador");
        }
        else{
            username.setText(userData);
        }

        userData = pref.getString("rol", "0");

        if((userData.equals("0"))){
            rol.setText("Mid");
        }
        else{
            rol.setText(userData);
        }

        userData = pref.getString("bio", "0");

        if((userData.equals("0"))){
            biografia.setText("Tu biografía");
        }
        else{
            biografia.setText(userData);
        }

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.editProfile);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), EditarPerfil.class);
                startActivity(intent);
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) view.findViewById(R.id.favorites);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ListaFavoritos.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);

        TextView username = getActivity().findViewById(R.id.nombreUsuario);
        TextView rol = getActivity().findViewById(R.id.rolPreferido);
        TextView biografia = getActivity().findViewById(R.id.biografia);

        String userData;
        userData = pref.getString("username", "0");

        if((userData.equals("0"))){
            username.setText("Invocador");
        }
        else{
            username.setText(userData);
        }

        userData = pref.getString("rol", "0");

        if((userData.equals("0"))){
            rol.setText("Mid");
        }
        else{
            rol.setText(userData);
        }

        userData = pref.getString("bio", "0");

        if((userData.equals("0"))){
            biografia.setText("Tu biografía");
        }
        else{
            biografia.setText(userData);
        }
    }
}