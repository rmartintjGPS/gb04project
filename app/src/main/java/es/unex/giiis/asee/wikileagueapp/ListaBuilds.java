package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.List;
import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class ListaBuilds extends AppCompatActivity implements MyAdapterBuilds.OnBuildListDeleteInteractionListener, MyAdapterBuilds.OnBuildListEditInteractionListener{


    private static final int ADD_BUILD_REQUEST = 016;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyAdapterBuilds mAdapter;
    ListaBuildsVM mViewModel;
    AppContainer appContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_builds);
        FloatingActionButton crearBuild = (FloatingActionButton) findViewById(R.id.crearBuild);
        crearBuild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListaBuilds.this, CrearBuild.class);
                startActivityForResult(intent,ADD_BUILD_REQUEST);
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.buildList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapterBuilds(this,this);
        mRecyclerView.setAdapter(mAdapter);

        appContainer = ((WikiLeague) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryListaBuilds).get(ListaBuildsVM.class);

        mViewModel.getAllBuilds().observe(this, builds -> {
            mAdapter.swap(builds);
            if (builds != null && builds.size() != 0) showBuildsDataView();
            else showLoading();
        });
    }

    private void showLoading(){
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void showBuildsDataView(){
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == ADD_BUILD_REQUEST){
            if (resultCode == RESULT_OK){
                Build build = new Build(data);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mViewModel.insert(build);
                    }
                });
            }
        }
    }
    @Override
    public void onBuildListDeleteInteraction(Build build) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mViewModel.delete(build);
            }
        });
    }

    @Override
    public void onBuildListEditInteraction(Build build, int position) {
        Intent intent = new Intent(ListaBuilds.this, EditarBuild.class);
        intent.putExtra("ID", build.getId());
        intent.putExtra("title", build.getTitle());
        intent.putExtra("champ", build.getChamp());
        intent.putExtra("item1", build.getItem1());
        intent.putExtra("item2", build.getItem2());
        intent.putExtra("item3", build.getItem3());
        intent.putExtra("item4", build.getItem4());
        intent.putExtra("item5", build.getItem5());
        intent.putExtra("item6", build.getItem6());
        startActivity(intent);
    }
}