package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class ListaFavoritos extends AppCompatActivity implements MyAdapterFavs.OnFavListInteractionListener, MyAdapterFavs.OnFavEditListInteractionListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyAdapterFavs mAdapter;
    ListaFavoritosVM mViewModel;
    AppContainer appContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_favoritos);

        appContainer = ((WikiLeague) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryListaFavoritos).get(ListaFavoritosVM.class);

        mRecyclerView = (RecyclerView) findViewById(R.id.favList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapterFavs(this, this);
        mRecyclerView.setAdapter(mAdapter);

        mViewModel.getAllFavs().observe(this, favs -> {
            mAdapter.swap(favs);
            if (favs != null && favs.size() != 0) showFavsDataView();
            else showLoading();
        });
    }

    @Override
    public void onFavEditListInteraction(FavChamp champ, int position) {
        Intent intent = new Intent(ListaFavoritos.this, EditarDominio.class);
        intent.putExtra("dominio",champ.getDominio());
        intent.putExtra("ID",champ.getId());
        intent.putExtra("name",champ.getName());
        intent.putExtra("image",champ.getImage());

        startActivity(intent);


    }
    @Override
    public void onFavListInteraction(FavChamp champ, int position) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mViewModel.delete(champ);
            }
        });
    }

    private void showLoading(){
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void showFavsDataView(){
        mRecyclerView.setVisibility(View.VISIBLE);
    }
}