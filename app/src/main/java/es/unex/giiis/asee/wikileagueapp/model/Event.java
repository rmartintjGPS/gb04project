package es.unex.giiis.asee.wikileagueapp.model;

import android.content.Intent;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "events")
public class Event {

    @Ignore
    public static final String EVENT_SEP = System.getProperty("line.separator");
    @Ignore
    public final static String ID = "ID";
    @Ignore
    public final static String TITLE = "title";
    @Ignore
    public final static String BODY = "body";

    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "title")
    private String title = new String();
    @ColumnInfo(name = "body")
    private String body = new String();

    @Ignore
    Event(String title, String body) {
        this.title = title;
        this.body = body;
    }

    @Ignore
    public Event(Intent intent) {
        id = intent.getLongExtra(Event.ID,0);
        title = intent.getStringExtra(Event.TITLE);
        body = intent.getStringExtra(Event.BODY);
    }

    public Event(long id, String title, String body) {
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    // Take a set of String data values and
    // package them for transport in an Intent

    public static void packageIntent(Intent intent, String title, String body) {

        intent.putExtra(Event.TITLE, title);
        intent.putExtra(Event.BODY, body);
    }

    public String toString() {
        return id + EVENT_SEP + title + EVENT_SEP + body;
    }
}
