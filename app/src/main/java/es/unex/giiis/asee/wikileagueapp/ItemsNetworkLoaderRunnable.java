package es.unex.giiis.asee.wikileagueapp;

import es.unex.giiis.asee.wikileagueapp.model.Item;

import java.io.IOException;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ItemsNetworkLoaderRunnable implements Runnable{

    private final OnItemsLoadedListener mOnItemsLoadedListener;

    public ItemsNetworkLoaderRunnable(OnItemsLoadedListener onItemsLoadedListener){
        mOnItemsLoadedListener = onItemsLoadedListener;
    }

    @Override
    public void run() {
        // Instanciación de Retrofit y llamada síncrona
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.pandascore.co/lol/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
        ItemsServices service = retrofit.create(ItemsServices.class);
        try {
            List<Item> items = service.listItems().execute().body();
            AppExecutors.getInstance().mainThread().execute(() -> mOnItemsLoadedListener.onItemsLoaded(items));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
