package es.unex.giiis.asee.wikileagueapp;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.Item;

public class ListaBuildsVM extends ViewModel {
    private final RoomRepository mRepository;
    private final LiveData<List<Build>> mBuilds;

    public ListaBuildsVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
        mBuilds = mRepository.getAllBuilds();
    }

    public LiveData<List<Build>> getAllBuilds() {
        return mBuilds;
    }

    public void delete(Build build) {
        mRepository.deleteBuild(build);
    }

    public void insert(Build build) {
        mRepository.insertBuild(build);
    }
}
