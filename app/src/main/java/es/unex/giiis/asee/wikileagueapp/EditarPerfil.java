package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class EditarPerfil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        TextView username = findViewById(R.id.username);
        RadioGroup rol = findViewById(R.id.rolGroup);
        TextView biografia = findViewById(R.id.biografiaText);

        String userData;
        userData = pref.getString("username", "0");

        if((userData.equals("0"))){
            username.setText("Invocador");
        }
        else{
            username.setText(userData);
        }

        userData = pref.getString("rol", "0");
        RadioButton mDefaultRolButton;
        switch (userData) {
            case "0":
            case "Mid":
                mDefaultRolButton = findViewById(R.id.rolMid);
                rol.check(mDefaultRolButton.getId());
                break;
            case "Top":
                mDefaultRolButton = findViewById(R.id.rolTop);
                rol.check(mDefaultRolButton.getId());
                break;
            case "Jungla":
                mDefaultRolButton = findViewById(R.id.rolJungla);
                rol.check(mDefaultRolButton.getId());
                break;
            case "ADCarry":
                mDefaultRolButton = findViewById(R.id.rolCarry);
                rol.check(mDefaultRolButton.getId());
                break;
            case "Support":
                mDefaultRolButton = findViewById(R.id.rolSup);
                rol.check(mDefaultRolButton.getId());
                break;
        }

        userData = pref.getString("bio", "0");

        if((userData.equals("0"))){
            biografia.setText("Tu biografía");
        }
        else{
            biografia.setText(userData);
        }

        final Button cancelButton =  findViewById(R.id.cancelarButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Button submitButton =  findViewById(R.id.guardarButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameString = username.getText().toString();
                String rolString = ((RadioButton)findViewById(rol.getCheckedRadioButtonId())).getText().toString();
                String bioString = biografia.getText().toString();

                if (!usernameString.equals("") && !bioString.equals("")) {
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("username", usernameString);
                    editor.putString("rol", rolString);
                    editor.putString("bio", bioString);
                    editor.commit();

                    finish();
                }
                else {
                    Toast.makeText(EditarPerfil.this, "Los campos deben ser rellenados.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}