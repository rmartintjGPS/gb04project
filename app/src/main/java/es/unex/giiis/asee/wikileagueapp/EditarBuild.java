package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class EditarBuild extends AppCompatActivity {

    EditarBuildVM mViewModel;
    AppContainer appContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_build);

        appContainer = ((WikiLeague) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryEditarBuilds).get(EditarBuildVM.class);

        long id = getIntent().getLongExtra("ID",0);
        String title = getIntent().getStringExtra("title");
        String champ = getIntent().getStringExtra("champ");
        String item1 = getIntent().getStringExtra("item1");
        String item2 = getIntent().getStringExtra("item2");
        String item3 = getIntent().getStringExtra("item3");
        String item4 = getIntent().getStringExtra("item4");
        String item5 = getIntent().getStringExtra("item5");
        String item6 = getIntent().getStringExtra("item6");

        EditText eTTitle = findViewById(R.id.editTitleBuild);
        EditText eTChamp = findViewById(R.id.EditChamp);
        EditText eTitem1 = findViewById(R.id.EditItem1);
        EditText eTitem2 = findViewById(R.id.EditItem2);
        EditText eTitem3 = findViewById(R.id.EditItem3);
        EditText eTitem4 = findViewById(R.id.EditItem4);
        EditText eTitem5 = findViewById(R.id.EditItem5);
        EditText eTitem6 = findViewById(R.id.EditItem6);

        eTTitle.setText(title);
        eTChamp.setText(champ);
        eTitem1.setText(item1);
        eTitem2.setText(item2);
        eTitem3.setText(item3);
        eTitem4.setText(item4);
        eTitem5.setText(item5);
        eTitem6.setText(item6);

        final Button cancelButton =  findViewById(R.id.cancelEditBuildButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Button submitButton =  findViewById(R.id.submitEditBuildButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titleString = eTTitle.getText().toString();
                String champString = eTChamp.getText().toString();
                String item1String = eTitem1.getText().toString();
                String item2String = eTitem2.getText().toString();
                String item3String = eTitem3.getText().toString();
                String item4String = eTitem4.getText().toString();
                String item5String = eTitem5.getText().toString();
                String item6String = eTitem6.getText().toString();

                if (!titleString.equals("") && !champString.equals("") && !item1String.equals("") && !item2String.equals("") && !item3String.equals("") && !item4String.equals("") && !item5String.equals("") && !item6String.equals("")) {
                    Build build = new Build(id, titleString, champString, item1String, item2String, item3String, item4String, item5String, item6String);
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            mViewModel.update(build);
                            finish();
                        }
                    });
                }
                else {
                    Toast.makeText(EditarBuild.this, "Los campos deben ser rellenados.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}