package es.unex.giiis.asee.wikileagueapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyAdapterFavs extends RecyclerView.Adapter<es.unex.giiis.asee.wikileagueapp.MyAdapterFavs.MyFavChampsViewHolder> {
    private List<FavChamp> mDataset;

    public interface OnFavListInteractionListener{
        public void onFavListInteraction(FavChamp fav, int position);
    }

    public MyAdapterFavs.OnFavListInteractionListener mDeleteListener;

    public interface OnFavEditListInteractionListener{
        public void onFavEditListInteraction(FavChamp fav, int position);
    }

    public MyAdapterFavs.OnFavEditListInteractionListener mEditListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyFavChampsViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ImageView mImageView;
        public TextView mTextViewDomain;
        public ImageView mImageViewDomain;
        public ImageButton mImageButton;
        public View mView;
        public ImageButton mImageEditButton;

        public FavChamp mItem;

        public MyFavChampsViewHolder(View v) {
            super(v);
            mView=v;
            mTextView = v.findViewById(R.id.champName);
            mImageView = v.findViewById(R.id.champIcon);
            mImageButton = v.findViewById(R.id.botonDeleteFav);
            mTextViewDomain = v.findViewById(R.id.champDomain);
            mImageViewDomain = v.findViewById(R.id.domainIcon);
            mImageEditButton = v.findViewById(R.id.botonEditFav);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapterFavs(MyAdapterFavs.OnFavListInteractionListener deleteListener, MyAdapterFavs.OnFavEditListInteractionListener editListener) {
        mDataset = new ArrayList<FavChamp>();
        mDeleteListener = deleteListener;
        mEditListener = editListener;
    }

    @Override
    public es.unex.giiis.asee.wikileagueapp.MyAdapterFavs.MyFavChampsViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                  int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frame_fav_list, parent, false);

        return new MyAdapterFavs.MyFavChampsViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyAdapterFavs.MyFavChampsViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.mTextView.setText(mDataset.get(position).getName());
        Picasso.get()
                .load(mDataset.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.mImageView);
        holder.mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteListener.onFavListInteraction(holder.mItem, position);
            }
        });
        holder.mTextViewDomain.setText(mDataset.get(position).getDominio());
        String url = "";
        switch (mDataset.get(position).getDominio()) {
            case "Principiante":
                url = "https://static.wikia.nocookie.net/leagueoflegends/images/d/d8/Champion_Mastery_Level_1_Flair.png/revision/latest/scale-to-width-down/105?cb=20150312005229";
                break;
            case "Amateur":
                url = "https://static.wikia.nocookie.net/leagueoflegends/images/b/b6/Champion_Mastery_Level_4_Flair.png/revision/latest/scale-to-width-down/120?cb=20200113041829";
                break;
            case "Avanzado":
                url = "https://static.wikia.nocookie.net/leagueoflegends/images/b/be/Champion_Mastery_Level_6_Flair.png/revision/latest/scale-to-width-down/120?cb=20200113041636";
                break;
            case "Legendario":
                url = "https://static.wikia.nocookie.net/leagueoflegends/images/7/7a/Champion_Mastery_Level_7_Flair.png/revision/latest/scale-to-width-down/120?cb=20200113041615";
                break;
        }
        Picasso.get()
                .load(url)
                .fit()
                .centerCrop()
                .into(holder.mImageViewDomain);

        holder.mImageEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditListener.onFavEditListInteraction(holder.mItem, position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swap(List<FavChamp> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}
