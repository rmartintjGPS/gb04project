package es.unex.giiis.asee.wikileagueapp;

import android.app.Application;
import android.content.Context;

import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class AppContainer {
    private WikiLeagueDatabase database;
    private ChampNetworkDataSource networkDataSource;
    private ItemNetworkDataSource networkDataSourceItem;
    public ApiRepository repository;
    public RoomRepository roomRepository;
    public ChampsFragmentVMFactory factory;
    public ItemsFragmentVMFactory factoryItems;
    public ListaBuildsVMFactory factoryListaBuilds;
    public EditarBuildVMFactory factoryEditarBuilds;
    public DetallesCampeonVMFactory factoryDetallesCampeon;
    public ListaFavoritosVMFactory factoryListaFavoritos;
    public EditarDominioVMFactory factoryEditarDominio;
    public ListaCompsVMFactory factoryListaComps;
    public EditarCompVMFactory factoryEditarComp;
    public EventsFragmentVMFactory factoryEventsFragment;

    public AppContainer(Context context){
        database = WikiLeagueDatabase.getInstance(context);
        networkDataSource = ChampNetworkDataSource.getInstance();
        networkDataSourceItem = ItemNetworkDataSource.getInstance();
        repository = ApiRepository.getInstance(database.getChampDao(), database.getItemDao(), networkDataSource, networkDataSourceItem);
        roomRepository = RoomRepository.getInstance(context);
        factory = new ChampsFragmentVMFactory(repository);
        factoryItems = new ItemsFragmentVMFactory(repository);
        factoryListaBuilds = new ListaBuildsVMFactory(context,roomRepository);
        factoryEditarBuilds = new EditarBuildVMFactory(context, roomRepository);
        factoryDetallesCampeon = new DetallesCampeonVMFactory(context, roomRepository);
        factoryListaFavoritos = new  ListaFavoritosVMFactory(context, roomRepository);
        factoryEditarDominio = new EditarDominioVMFactory(context, roomRepository);
        factoryListaComps = new ListaCompsVMFactory(context, roomRepository);
        factoryEditarComp = new EditarCompVMFactory(context, roomRepository);
        factoryEventsFragment = new EventsFragmentVMFactory(context, roomRepository);
    }
}
