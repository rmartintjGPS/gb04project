package es.unex.giiis.asee.wikileagueapp;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import es.unex.giiis.asee.wikileagueapp.model.Champion;

public class ChampNetworkDataSource {
    private static final String LOG_TAG = ChampNetworkDataSource.class.getSimpleName();
    private static ChampNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<Champion[]> mDownloadedChamps;

    private ChampNetworkDataSource() {
        mDownloadedChamps = new MutableLiveData<>();
    }

    public synchronized static ChampNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new ChampNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<Champion[]> getCurrentChamps() {
        return mDownloadedChamps;
    }

    /**
     * Gets the newest champs
     */
    public void fetchChamps() {
        Log.d(LOG_TAG, "Fetch champs started");
        // Get data from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new ChampsNetworkLoaderRunnable(champs -> mDownloadedChamps.postValue(champs.toArray(new Champion[0]))));
    }
}
