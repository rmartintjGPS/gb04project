package es.unex.giiis.asee.wikileagueapp;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.model.Event;
import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import es.unex.giiis.asee.wikileagueapp.roomdb.BuildDao;
import es.unex.giiis.asee.wikileagueapp.roomdb.CompDao;
import es.unex.giiis.asee.wikileagueapp.roomdb.EventDao;
import es.unex.giiis.asee.wikileagueapp.roomdb.FavChampDao;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class RoomRepository {
    private static final String LOG_TAG = RoomRepository.class.getSimpleName();

    private static RoomRepository sInstance;
    private BuildDao buildDao;
    private FavChampDao favChampDao;
    private CompDao compDao;
    private EventDao eventDao;
    private LiveData<List<Build>> allBuilds;
    private LiveData<List<FavChamp>> allFavs;
    private LiveData<List<Comp>> allComps;
    private LiveData<List<Event>> allEvents;


    public RoomRepository(Context context) {
        WikiLeagueDatabase database = WikiLeagueDatabase.getInstance(context);
        buildDao = database.getBuildDao();
        favChampDao = database.getFavChampDao();
        compDao = database.getCompDao();
        eventDao = database.getDao();
        allBuilds = buildDao.getAll();
        allFavs = favChampDao.getAllFavChamps();
        allComps = compDao.getAll();
        allEvents = eventDao.getAll();
    }

    public synchronized static RoomRepository getInstance(Context context) {
        Log.d(LOG_TAG, "Getting the room repository");
        if (sInstance == null) {
            sInstance = new RoomRepository(context);
            Log.d(LOG_TAG, "Made new room repository");
        }
        return sInstance;
    }

    public void insertBuild (Build build) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                buildDao.insert(build);
            }
        });
    }

    public void updateBuild (Build build) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                buildDao.update(build);
            }
        });
    }

    public void deleteBuild (Build build) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                buildDao.delete(build);
            }
        });
    }

    public LiveData<List<Build>> getAllBuilds() {
        return allBuilds;
    }

    public void insertFavChamp (FavChamp favChamp) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                favChampDao.insert(favChamp);
            }
        });
    }

    public void updateFavChamp (FavChamp favChamp) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                favChampDao.update(favChamp);
            }
        });
    }

    public void deleteFavChamp (FavChamp favChamp) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                favChampDao.delete(favChamp);
            }
        });
    }

    public int getNumberFavChampByName (String name) {
        return favChampDao.getNumberFavChampByName(name);
    }

    public LiveData<List<FavChamp>> getAllFavs() {
        return allFavs;
    }

    public void insertComp (Comp comp) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                compDao.insert(comp);
            }
        });
    }

    public void updateComp (Comp comp) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                compDao.update(comp);
            }
        });
    }

    public void deleteComp (Comp comp) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                compDao.delete(comp);
            }
        });
    }

    public LiveData<List<Comp>> getAllComps() {
        return allComps;
    }

    public LiveData<List<Event>> getAllEvents() {
        return allEvents;
    }

    public void deleteAllEvents() {
        eventDao.deleteAll();
    }

    public void insertEvent (Event event) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                eventDao.insert(event);
            }
        });
    }
}
