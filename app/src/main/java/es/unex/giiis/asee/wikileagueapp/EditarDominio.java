package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

import static java.lang.Integer.parseInt;

public class EditarDominio extends AppCompatActivity {

    EditarDominioVM mViewModel;
    AppContainer appContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_dominio);

        appContainer = ((WikiLeague) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryEditarDominio).get(EditarDominioVM.class);

        String dominio = getIntent().getStringExtra("dominio");
        String name = getIntent().getStringExtra("name");
        String image = getIntent().getStringExtra("image");
        Long ID = getIntent().getLongExtra("ID" , 0);

        RadioButton mDefaultMaestriaButton;
        RadioGroup rol = findViewById(R.id.dominioGroup);
        switch (dominio) {
            case "Principiante":
                mDefaultMaestriaButton = findViewById(R.id.principiante);
                rol.check(mDefaultMaestriaButton.getId());
                break;
            case "Amateur":
                mDefaultMaestriaButton = findViewById(R.id.amateur);
                rol.check(mDefaultMaestriaButton.getId());
                break;
            case "Avanzado":
                mDefaultMaestriaButton = findViewById(R.id.avanzado);
                rol.check(mDefaultMaestriaButton.getId());
                break;
            case "Legendario":
                mDefaultMaestriaButton = findViewById(R.id.legendario);
                rol.check(mDefaultMaestriaButton.getId());

        }

        final Button cancelButton =  findViewById(R.id.cancelarMaestriaButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Button submitButton =  findViewById(R.id.guardarMaestriaButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dominioString = ((RadioButton) findViewById(rol.getCheckedRadioButtonId())).getText().toString();
                FavChamp champ = new FavChamp(ID, name, image, dominioString);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mViewModel.update(champ);
                        finish();
                    }
                });
            }
        });
    }
}