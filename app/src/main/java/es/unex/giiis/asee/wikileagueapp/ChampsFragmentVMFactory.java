package es.unex.giiis.asee.wikileagueapp;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ChampsFragmentVMFactory extends ViewModelProvider.NewInstanceFactory {
    private final ApiRepository mRepository;

    public ChampsFragmentVMFactory(ApiRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ChampsFragmentVM(mRepository);
    }
}
