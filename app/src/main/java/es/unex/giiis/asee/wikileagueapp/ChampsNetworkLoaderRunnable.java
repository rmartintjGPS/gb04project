package es.unex.giiis.asee.wikileagueapp;

import java.io.IOException;
import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Champion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChampsNetworkLoaderRunnable implements Runnable{

    private final OnChampsLoadedListener mOnChampsLoadedListener;

    public ChampsNetworkLoaderRunnable(OnChampsLoadedListener onChampsLoadedListener){
        mOnChampsLoadedListener = onChampsLoadedListener;
    }

    @Override
    public void run() {
        // Instanciación de Retrofit y llamada síncrona
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.pandascore.co/lol/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChampsServices service = retrofit.create(ChampsServices.class);
        try {
            List<Champion> champs = service.listChamps().execute().body();
            AppExecutors.getInstance().mainThread().execute(() -> mOnChampsLoadedListener.onChampsLoaded(champs));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

