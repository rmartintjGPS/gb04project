package es.unex.giiis.asee.wikileagueapp;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.squareup.picasso.Picasso;
import java.util.List;
import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class DetallesCampeon extends AppCompatActivity {

    DetallesCampeonVM mViewModel;
    AppContainer appContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_campeon);

        appContainer = ((WikiLeague) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryDetallesCampeon).get(DetallesCampeonVM.class);

        String name = getIntent().getStringExtra("name");
        String splashart = getIntent().getStringExtra("splashart");
        String dominio = "Principiante";
        Double armor = getIntent().getDoubleExtra("armor",0);
        Double ad = getIntent().getDoubleExtra("ad",0);
        Double range = getIntent().getDoubleExtra("range",0);
        Double crit = getIntent().getDoubleExtra("crit",0);
        Double hp = getIntent().getDoubleExtra("hp",0);
        Double hpregen = getIntent().getDoubleExtra("hpregen",0);
        Double movespeed = getIntent().getDoubleExtra("movespeed",0);
        Double mana = getIntent().getDoubleExtra("mana",0);
        Double manaregen = getIntent().getDoubleExtra("manaregen",0);
        Double spellblock = getIntent().getDoubleExtra("spellblock",0);

        String image = getIntent().getStringExtra("image");

        TextView nombre = findViewById(R.id.nombreCampeon);
        nombre.setText(name);
        ImageView imagen = findViewById(R.id.imagenCampeon);
        Picasso.get()
                .load(splashart)
                .fit()
                .centerCrop()
                .into(imagen);
        TextView armadura = findViewById(R.id.armaduraCampeon);
        armadura.setText("Armadura: "+ armor);
        TextView danio = findViewById(R.id.adCampeon);
        danio.setText("Daño de ataque: "+ ad);
        TextView rango = findViewById(R.id.rangoCampeon);
        rango.setText("Rango: "+ range);
        TextView critico = findViewById(R.id.criticoCampeon);
        critico.setText("Probabilidad de crítico: "+ crit);
        TextView vida = findViewById(R.id.vidaCampeon);
        vida.setText("Puntos de salud: "+ hp);
        TextView vidaRegen = findViewById(R.id.regeneracionVidaCampeon);
        vidaRegen.setText("Regeneración de vida: "+ hpregen);
        TextView velocidad = findViewById(R.id.velocidadCampeon);
        velocidad.setText("Velocidad: "+ movespeed);
        TextView mana2 = findViewById(R.id.manaCampeon);
        mana2.setText("Mana: "+ mana);
        TextView manaregen2 = findViewById(R.id.regeneracionManaCampeon);
        manaregen2.setText("Regen. de mana: "+ manaregen);
        TextView resistenciaMagica = findViewById(R.id.resistenciaMagicaCampeon);
        resistenciaMagica.setText("Resistencia mágica: "+ spellblock);
        ImageButton botonFav = findViewById(R.id.botonFav);
        botonFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        int favChamps = mViewModel.getNumberFavChampByName(name);
                        if (favChamps == 0) {
                            FavChamp champ = new FavChamp(name, image, dominio);
                            mViewModel.insert(champ);
                            runOnUiThread(() -> Toast.makeText(DetallesCampeon.this, "Se ha añadido el campeón a la lista de favoritos.", Toast.LENGTH_SHORT).show());
                        }
                        else {
                            runOnUiThread(()->Toast.makeText(DetallesCampeon.this, "El campeón ya existe en la lista de favoritos.", Toast.LENGTH_SHORT).show());
                        }
                    }
                });
            }
        });
    }
}