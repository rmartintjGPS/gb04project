package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class EditarComp extends AppCompatActivity {

    EditarCompVM mViewModel;
    AppContainer appContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_comp);

        appContainer = ((WikiLeague) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryEditarComp).get(EditarCompVM.class);

        long id = getIntent().getLongExtra("ID",0);
        String title = getIntent().getStringExtra("title");
        String champ1 = getIntent().getStringExtra("champ1");
        String champ2 = getIntent().getStringExtra("champ2");
        String champ3 = getIntent().getStringExtra("champ3");
        String champ4 = getIntent().getStringExtra("champ4");
        String champ5 = getIntent().getStringExtra("champ5");
        String comment = getIntent().getStringExtra("comment");

        EditText eTTitle = findViewById(R.id.editTitleComp);
        EditText eTChamp1 = findViewById(R.id.EditChamp1);
        EditText eTChamp2 = findViewById(R.id.EditChamp2);
        EditText eTChamp3 = findViewById(R.id.EditChamp3);
        EditText eTChamp4 = findViewById(R.id.EditChamp4);
        EditText eTChamp5 = findViewById(R.id.EditChamp5);
        EditText eTComment = findViewById(R.id.EditComentario);

        eTTitle.setText(title);
        eTChamp1.setText(champ1);
        eTChamp2.setText(champ2);
        eTChamp3.setText(champ3);
        eTChamp4.setText(champ4);
        eTChamp5.setText(champ5);
        eTComment.setText(comment);

        final Button cancelButton =  findViewById(R.id.cancelEditCompButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Button submitButton =  findViewById(R.id.submitEditCompButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titleString = eTTitle.getText().toString();
                String champ1String = eTChamp1.getText().toString();
                String champ2String = eTChamp2.getText().toString();
                String champ3String = eTChamp3.getText().toString();
                String champ4String = eTChamp4.getText().toString();
                String champ5String = eTChamp5.getText().toString();
                String commentString = eTComment.getText().toString();

                if (!titleString.equals("") && !champ1String.equals("") && !champ2String.equals("") && !champ3String.equals("") && !champ4String.equals("") && !champ5String.equals("")) {
                    Comp comp = new Comp(id, titleString, champ1String, champ2String, champ3String, champ4String, champ5String, commentString);
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            mViewModel.update(comp);
                            finish();
                        }
                    });
                }
                else {
                    Toast.makeText(EditarComp.this, "Los campos deben ser rellenados.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}