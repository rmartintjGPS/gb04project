package es.unex.giiis.asee.wikileagueapp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Item;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ItemDao {
    @Query("SELECT * FROM items")
    public LiveData<List<Item>> getAll();
    @Query("DELETE FROM items")
    public void deleteAll();
    @Insert(onConflict = REPLACE)
    public void bulkInsert(List<Item> items);
    @Query("SELECT count(*) FROM items")
    int getNumberItemsInCache();
}
