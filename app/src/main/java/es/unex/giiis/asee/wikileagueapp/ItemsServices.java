package es.unex.giiis.asee.wikileagueapp;

import es.unex.giiis.asee.wikileagueapp.model.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ItemsServices {
    @GET("items?token=EDZwdB0PpRVDcWMnyoE9Y1NPO7T-a5EBmqVeSac88-ytL5mgLMY")
    Call<List<Item>> listItems();
}