package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Event;

public class EventsFragmentVM extends ViewModel {
    private final RoomRepository mRepository;
    private final LiveData<List<Event>> mEvents;

    public EventsFragmentVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
        mEvents = mRepository.getAllEvents();
    }

    public LiveData<List<Event>> getAllEvents() {
        return mEvents;
    }

    public void deleteAll() {mRepository.deleteAllEvents();}

    public void insert(Event event) {mRepository.insertEvent(event);}
}
