package es.unex.giiis.asee.wikileagueapp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import java.util.List;


@Dao
public interface FavChampDao {
    @Query("SELECT count(*) FROM favChamps WHERE name = :name")
    public int getNumberFavChampByName(String name);

    @Query("SELECT * FROM favChamps")
    public LiveData<List<FavChamp>> getAllFavChamps();

    @Insert
    public long insert(FavChamp favChamp);

    @Delete
    public void delete(FavChamp champ);

    @Update
    public void update(FavChamp favChamp);
}
