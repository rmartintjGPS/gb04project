
package es.unex.giiis.asee.wikileagueapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

@Entity(tableName="items")
public class Item {

    @SerializedName("flat_armor_mod")
    @Expose
    @ColumnInfo
    private Integer flatArmorMod;
    @SerializedName("flat_crit_chance_mod")
    @Expose
    @ColumnInfo
    private Integer flatCritChanceMod;
    @SerializedName("flat_hp_pool_mod")
    @Expose
    @ColumnInfo
    private Integer flatHpPoolMod;
    @SerializedName("flat_hp_regen_mod")
    @Expose
    @ColumnInfo
    private Integer flatHpRegenMod;
    @SerializedName("flat_magic_damage_mod")
    @Expose
    @ColumnInfo
    private Integer flatMagicDamageMod;
    @SerializedName("flat_movement_speed_mod")
    @Expose
    @ColumnInfo
    private Integer flatMovementSpeedMod;
    @SerializedName("flat_mp_pool_mod")
    @Expose
    @ColumnInfo
    private Integer flatMpPoolMod;
    @SerializedName("flat_mp_regen_mod")
    @Expose
    @ColumnInfo
    private Integer flatMpRegenMod;
    @SerializedName("flat_physical_damage_mod")
    @Expose
    @ColumnInfo
    private Integer flatPhysicalDamageMod;
    @SerializedName("flat_spell_block_mod")
    @Expose
    @Ignore
    private Object flatSpellBlockMod;
    @SerializedName("gold_base")
    @Expose
    @ColumnInfo
    private Integer goldBase;
    @SerializedName("gold_purchasable")
    @Expose
    @ColumnInfo
    private Boolean goldPurchasable;
    @SerializedName("gold_sell")
    @Expose
    @ColumnInfo
    private Integer goldSell;
    @SerializedName("gold_total")
    @Expose
    @ColumnInfo
    private Integer goldTotal;
    @SerializedName("id")
    @Expose
    @PrimaryKey
    private Integer id;
    @SerializedName("image_url")
    @Expose
    @ColumnInfo
    private String imageUrl;
    @SerializedName("is_trinket")
    @Expose
    @ColumnInfo
    private Boolean isTrinket;
    @SerializedName("name")
    @Expose
    @ColumnInfo
    private String name;
    @SerializedName("percent_attack_speed_mod")
    @Expose
    @ColumnInfo
    private Integer percentAttackSpeedMod;
    @SerializedName("percent_life_steal_mod")
    @Expose
    @ColumnInfo
    private Integer percentLifeStealMod;
    @SerializedName("percent_movement_speed_mod")
    @Expose
    @ColumnInfo
    private Integer percentMovementSpeedMod;
    @SerializedName("videogame_versions")
    @Expose
    @Ignore
    private List<String> videogameVersions = null;

    public Integer getFlatArmorMod() {
        return flatArmorMod;
    }

    public void setFlatArmorMod(Integer flatArmorMod) {
        this.flatArmorMod = flatArmorMod;
    }

    public Integer getFlatCritChanceMod() {
        return flatCritChanceMod;
    }

    public void setFlatCritChanceMod(Integer flatCritChanceMod) {
        this.flatCritChanceMod = flatCritChanceMod;
    }

    public Integer getFlatHpPoolMod() {
        return flatHpPoolMod;
    }

    public void setFlatHpPoolMod(Integer flatHpPoolMod) {
        this.flatHpPoolMod = flatHpPoolMod;
    }

    public Integer getFlatHpRegenMod() {
        return flatHpRegenMod;
    }

    public void setFlatHpRegenMod(Integer flatHpRegenMod) {
        this.flatHpRegenMod = flatHpRegenMod;
    }

    public Integer getFlatMagicDamageMod() {
        return flatMagicDamageMod;
    }

    public void setFlatMagicDamageMod(Integer flatMagicDamageMod) {
        this.flatMagicDamageMod = flatMagicDamageMod;
    }

    public Integer getFlatMovementSpeedMod() {
        return flatMovementSpeedMod;
    }

    public void setFlatMovementSpeedMod(Integer flatMovementSpeedMod) {
        this.flatMovementSpeedMod = flatMovementSpeedMod;
    }

    public Integer getFlatMpPoolMod() {
        return flatMpPoolMod;
    }

    public void setFlatMpPoolMod(Integer flatMpPoolMod) {
        this.flatMpPoolMod = flatMpPoolMod;
    }

    public Integer getFlatMpRegenMod() {
        return flatMpRegenMod;
    }

    public void setFlatMpRegenMod(Integer flatMpRegenMod) {
        this.flatMpRegenMod = flatMpRegenMod;
    }

    public Integer getFlatPhysicalDamageMod() {
        return flatPhysicalDamageMod;
    }

    public void setFlatPhysicalDamageMod(Integer flatPhysicalDamageMod) {
        this.flatPhysicalDamageMod = flatPhysicalDamageMod;
    }

    public Object getFlatSpellBlockMod() {
        return flatSpellBlockMod;
    }

    public void setFlatSpellBlockMod(Object flatSpellBlockMod) {
        this.flatSpellBlockMod = flatSpellBlockMod;
    }

    public Integer getGoldBase() {
        return goldBase;
    }

    public void setGoldBase(Integer goldBase) {
        this.goldBase = goldBase;
    }

    public Boolean getGoldPurchasable() {
        return goldPurchasable;
    }

    public void setGoldPurchasable(Boolean goldPurchasable) {
        this.goldPurchasable = goldPurchasable;
    }

    public Integer getGoldSell() {
        return goldSell;
    }

    public void setGoldSell(Integer goldSell) {
        this.goldSell = goldSell;
    }

    public Integer getGoldTotal() {
        return goldTotal;
    }

    public void setGoldTotal(Integer goldTotal) {
        this.goldTotal = goldTotal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getIsTrinket() {
        return isTrinket;
    }

    public void setIsTrinket(Boolean isTrinket) {
        this.isTrinket = isTrinket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPercentAttackSpeedMod() {
        return percentAttackSpeedMod;
    }

    public void setPercentAttackSpeedMod(Integer percentAttackSpeedMod) {
        this.percentAttackSpeedMod = percentAttackSpeedMod;
    }

    public Integer getPercentLifeStealMod() {
        return percentLifeStealMod;
    }

    public void setPercentLifeStealMod(Integer percentLifeStealMod) {
        this.percentLifeStealMod = percentLifeStealMod;
    }

    public Integer getPercentMovementSpeedMod() {
        return percentMovementSpeedMod;
    }

    public void setPercentMovementSpeedMod(Integer percentMovementSpeedMod) {
        this.percentMovementSpeedMod = percentMovementSpeedMod;
    }

    public List<String> getVideogameVersions() {
        return videogameVersions;
    }

    public void setVideogameVersions(List<String> videogameVersions) {
        this.videogameVersions = videogameVersions;
    }

}
