package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.model.Event;

public class CrearComp extends AppCompatActivity {

    private EditText mTitle;
    private EditText mChamp1;
    private EditText mChamp2;
    private EditText mChamp3;
    private EditText mChamp4;
    private EditText mChamp5;
    private EditText mComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_comp);
        mTitle = findViewById(R.id.titleComp);
        mChamp1 = findViewById(R.id.Champ1);
        mChamp2 = findViewById(R.id.Champ2);
        mChamp3 = findViewById(R.id.Champ3);
        mChamp4 = findViewById(R.id.Champ4);
        mChamp5 = findViewById(R.id.Champ5);
        mComment = findViewById(R.id.Comentario);

        final Button cancelButton =  findViewById(R.id.cancelCompButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent data = new Intent();
                setResult(RESULT_CANCELED, data);
                finish();

            }
        });

        final Button submitButton =  findViewById(R.id.submitCompButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titleString = mTitle.getText().toString();
                String champ1String = mChamp1.getText().toString();
                String champ2String = mChamp2.getText().toString();
                String champ3String = mChamp3.getText().toString();
                String champ4String = mChamp4.getText().toString();
                String champ5String = mChamp5.getText().toString();
                String comentarioString = mComment.getText().toString();

                if (!titleString.equals("") && !champ1String.equals("") && !champ2String.equals("") && !champ3String.equals("") && !champ4String.equals("") && !champ5String.equals("")) {
                    Intent data = new Intent();
                    Comp.packageIntent(data, titleString, champ1String, champ2String, champ3String, champ4String, champ5String, comentarioString);
                    setResult(RESULT_OK, data);
                    finish();
                }
                else {
                    Toast.makeText(CrearComp.this, "Los campos deben ser rellenados.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}