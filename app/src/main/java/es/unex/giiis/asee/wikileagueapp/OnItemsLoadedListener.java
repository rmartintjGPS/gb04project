package es.unex.giiis.asee.wikileagueapp;

import es.unex.giiis.asee.wikileagueapp.model.Item;

import java.util.List;

public interface OnItemsLoadedListener {
    public void onItemsLoaded(List<Item> items);
}