package es.unex.giiis.asee.wikileagueapp;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ItemsFragmentVMFactory extends ViewModelProvider.NewInstanceFactory {
    private final ApiRepository mRepository;

    public ItemsFragmentVMFactory(ApiRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ItemsFragmentVM(mRepository);
    }
}
