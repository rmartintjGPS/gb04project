package es.unex.giiis.asee.wikileagueapp;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Champion;

public class ChampsFragmentVM extends ViewModel {

    private final ApiRepository mRepository;
    private final LiveData<List<Champion>> mChamps;

    public ChampsFragmentVM(ApiRepository repository) {
        mRepository = repository;
        mChamps = mRepository.getCurrentChamps();
    }

    public void updateRepo(){
        mRepository.updateRepo();
    }

    public void onRefresh() {
        mRepository.doFetchChamps();
    }

    public LiveData<List<Champion>> getChamps() {
        return mChamps;
    }

}
