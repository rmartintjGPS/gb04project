package es.unex.giiis.asee.wikileagueapp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Champion;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ChampDao {
    @Query("SELECT * FROM champs")
    public LiveData<List<Champion>> getAll();
    @Query("DELETE FROM champs")
    public void deleteAll();
    @Insert(onConflict = REPLACE)
    public void bulkInsert(List<Champion> champions);
    @Query("SELECT count(*) FROM champs")
    int getNumberChampsInCache();
}
