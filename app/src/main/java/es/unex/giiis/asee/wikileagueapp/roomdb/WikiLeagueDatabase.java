package es.unex.giiis.asee.wikileagueapp.roomdb;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.Champion;
import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.model.Event;
import es.unex.giiis.asee.wikileagueapp.model.FavChamp;
import es.unex.giiis.asee.wikileagueapp.model.Item;


@Database(entities = {Event.class, FavChamp.class , Comp.class, Build.class, Champion.class, Item.class}, version = 16)
public abstract class WikiLeagueDatabase extends RoomDatabase {
    private static WikiLeagueDatabase instance;

    public static WikiLeagueDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, WikiLeagueDatabase.class, "wikiLeague.db").fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    public abstract EventDao getDao();
    public abstract FavChampDao getFavChampDao();
    public abstract CompDao getCompDao();
    public abstract BuildDao getBuildDao();
    public abstract ChampDao getChampDao();
    public abstract ItemDao getItemDao();
}