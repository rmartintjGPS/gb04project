package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.FavChamp;

public class DetallesCampeonVM extends ViewModel {
    private final RoomRepository mRepository;

    public DetallesCampeonVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
    }

    public void insert(FavChamp favChamp) {
        mRepository.insertFavChamp(favChamp);
    }

    public int getNumberFavChampByName(String name) {
        return mRepository.getNumberFavChampByName(name);
    }
}
