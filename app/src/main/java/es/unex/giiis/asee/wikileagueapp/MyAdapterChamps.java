package es.unex.giiis.asee.wikileagueapp;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import es.unex.giiis.asee.wikileagueapp.model.Champion;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapterChamps extends RecyclerView.Adapter<es.unex.giiis.asee.wikileagueapp.MyAdapterChamps.MyViewHolder> {
    private List<Champion> mDataset;

    public interface OnListInteractionListener{
        public void onListInteraction(Champion champ);
    }

    public OnListInteractionListener mListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ImageView mImageView;
        public View mView;

        public Champion mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;
            mTextView = v.findViewById(R.id.champNameLable);
            mImageView = v.findViewById(R.id.champIconLable);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapterChamps(List<Champion> myDataset, OnListInteractionListener listener) {
        mListener = listener;
        mDataset = myDataset;
    }

    @Override
    public es.unex.giiis.asee.wikileagueapp.MyAdapterChamps.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                            int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frame_champ_list, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.mTextView.setText(mDataset.get(position).getName());
        Picasso.get()
                .load(mDataset.get(position).getImageUrl())
                .fit()
                .centerCrop()
                .into(holder.mImageView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListInteraction(holder.mItem);
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swap(List<Champion> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}