package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import es.unex.giiis.asee.wikileagueapp.model.Build;

public class CrearBuild extends AppCompatActivity {

    private EditText mTitle;
    private EditText mChamp;
    private EditText mitem1;
    private EditText mitem2;
    private EditText mitem3;
    private EditText mitem4;
    private EditText mitem5;
    private EditText mitem6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_build);
        mTitle = findViewById(R.id.titleBuild);
        mChamp = findViewById(R.id.Champ);
        mitem1 = findViewById(R.id.Item1);
        mitem2 = findViewById(R.id.Item2);
        mitem3 = findViewById(R.id.Item3);
        mitem4 = findViewById(R.id.Item4);
        mitem5 = findViewById(R.id.Item5);
        mitem6 = findViewById(R.id.Item6);

        final Button cancelButton = findViewById(R.id.cancelBuildButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent data = new Intent();
                setResult(RESULT_CANCELED, data);
                finish();

            }
        });

        final Button submitButton = findViewById(R.id.submitBuildButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titleString = mTitle.getText().toString();
                String champString = mChamp.getText().toString();
                String item1String = mitem1.getText().toString();
                String item2String = mitem2.getText().toString();
                String item3String = mitem3.getText().toString();
                String item4String = mitem4.getText().toString();
                String item5String = mitem5.getText().toString();
                String item6String = mitem6.getText().toString();

                if (!titleString.equals("") && !champString.equals("") && !item1String.equals("") && !item2String.equals("") && !item3String.equals("") && !item4String.equals("") && !item5String.equals("") && !item6String.equals("")) {
                    Intent data = new Intent();
                    Build.packageIntent(data, titleString, champString, item1String, item2String, item3String, item4String, item5String, item6String);
                    setResult(RESULT_OK, data);
                    finish();
                } else {
                    Toast.makeText(CrearBuild.this, "Los campos deben ser rellenados.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

