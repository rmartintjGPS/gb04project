package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import es.unex.giiis.asee.wikileagueapp.model.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapterItems extends BaseAdapter {
    private List<Item> mDataset;
    private Context context;

    public MyAdapterItems(Context context, List<Item> items) {
        this.context = context;
        this.mDataset = items;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.frame_item_list, parent, false);
        }
        Item currentItem = (Item) getItem(position);

        TextView textViewItemName = (TextView) convertView.findViewById(R.id.itemNameLabel);
        ImageView imageViewItemIcon = (ImageView) convertView.findViewById(R.id.itemIconLabel);

        textViewItemName.setText(currentItem.getName());
        Picasso.get()
                .load(currentItem.getImageUrl())
                .fit()
                .centerCrop()
                .into(imageViewItemIcon);
        return convertView;
    }

    public void swap(List<Item> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}